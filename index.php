<!DOCTYPE html>
<html lang="zh_CN">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width:device-width, initial-scale=1, maximumu-scale=1, user-scalable=no" />
        <meta name="keywords" content="棉花糖英雄, Cotton, Hero, 游戏" />
        <meta name="description" content="【这不再老套的英雄救美的故事】公主目睹王子和大叔的基情伤心欲绝，最终小宇宙爆发，诅咒了王子并且掳走了大叔。当王子再度苏醒过来时就此告别了高富帅状态变成了棉花糖小人……" />
        <meta name="company" content="上海闲景网络科技有限公司" />
        <title>棉花糖英雄 - 棉花糖工作室</title>
        <link rel="stylesheet" href="css/style.css" />
        <link rel="icon" href="favicon.ico" type="text/x-icon" />

    </head>
    <body>
    <!--[if lt IE 9]>
        <div class="error chromeframe">您的浏览器版本<strong>很旧很旧</strong>，为了正常地访问网站，请升级您的浏览器 <a target="_blank" href="http://browsehappy.com">立即升级</a></div>
    <![endif]-->
        <div class="container">
            <!--div class="nav fixed">
                <ul>
                    <li><a href="">立于不败之</a></li>
                </ul>
            </div-->
            <div class="main">
                <h1 class="hidden">棉花糖英雄</h1>
                <h1><img alt="棉花糖英雄" src="images/title.png"></h1>
                <span>王子与大叔的基情冒险</span>
                <div class="clear"></div>
                <div class="download">
                    <a href="http://appstore.com/棉花糖英雄" target="_blank"><img alt="App Store" src="images/appstore.png"/></a>
                    <a href="http://appstore.com/izju" target="_blank" disable><img alt="Google Play" src="images/googleplay.png"/></a>
                </div>
            </div>
            <div class="screenshot">
                <a href="images/4.png"><img alt="screenshot0" src="images/4.png"/></a>
                <a href="images/5.png"><img alt="screenshot0" src="images/5.png"/></a>
                <a href="images/6.png"><img alt="screenshot0" src="images/6.png"/></a>
            </div>
        </div>
        <div class="footer">
            <footer>
                <strong>棉花糖工作室</strong> &copy 2014，版权所有 |
                <a href="mailto:cottonhero@ixianjing.com">联系我们</a> |
                沪ICP备14033913号-3
            </footer>
        </div>
        <script src="http://cdn.bootcss.com/jquery/2.1.1-rc2/jquery.min.js"></script>
        <link href="http://cdn.bootcss.com/fancybox/2.1.5/jquery.fancybox.min.css" rel="stylesheet">
        <script src="http://cdn.bootcss.com/fancybox/2.1.5/jquery.fancybox.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $(".screenshot a").fancybox();
            });
        </script>
    </body>
</html>
